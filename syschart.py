#!/usr/bin/env python3

from PIL import Image, ImageFont, ImageDraw
from easyhid import Enumeration
from time import sleep
import signal
import sys
import psutil
import subprocess
import json

def signal_handler(sig, frame):
    try:
        # Blank screen on shutdown
        dev.send_feature_report(bytearray([0x61] + [0x00] * 641))
        dev.close()
        print("\n")
        sys.exit(0)
    except:
        sys.exit(0)

# Set up ctrl-c handler
signal.signal(signal.SIGINT, signal_handler)

# Stores an enumeration of all the connected USB HID devices
en = Enumeration()
# Return a list of devices based on the search parameters / Hardcoded to Apex 7
devices = en.find(vid=0x1038, pid=0x1612, interface=1)
if not devices:
    devices = en.find(vid=0x1038, pid=0x1618, interface=1)
if not devices:
    print("No devices found, exiting.")
    sys.exit(0)

# Use first device found with vid/pid
dev = devices[0]

print("Found: " + dev.product_string)
print("Press Ctrl-C to exit.\n")
try:
	dev.open()
except:
	exit()

im = Image.new('1', (128,40))
draw = ImageDraw.Draw(im)

cpu_history = []
mem_history = []
gpu_history = []
gpu_mem_history = []
pixel_width = 128
pixel_height = 20

currentGraph = 0
show_cpu = 0
show_mem = 1
show_gpu = 2
show_gpu_mem = 3
gpustatscommand = "amdgpu_top --json -d"

while(1):
    # use a truetype font
    draw.rectangle([(0,0),(128,40)], fill=0)
    font = ImageFont.truetype("/home/rd/Dev/Personal/steelseries-oled/OpenSans-Regular.ttf", 10)

    # CPU stats
    cpupercent = psutil.cpu_percent(interval=1);
    mempercent = psutil.virtual_memory().percent
    temperatures = psutil.sensors_temperatures()
    
    cpu_history.append(cpupercent)
    if len(cpu_history) == pixel_width:
        cpu_history.pop(0)
    
    mem_history.append(mempercent)
    if len(mem_history) == pixel_width:
        mem_history.pop(0)
    
    # GPU stats
    process = subprocess.run(gpustatscommand, shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
    gpustats = json.loads(process.stdout)
    currentgpustats = gpustats[0]
    gpupercent = currentgpustats['gpu_activity']['GFX']['value']
    gpu_history.append(gpupercent)
    if len(gpu_history) == pixel_width:
        gpu_history.pop(0)
    
    gpumem = currentgpustats['gpu_activity']['Memory']['value']
    gpu_mem_history.append(gpumem)
    if len(gpu_mem_history) == pixel_width:
        gpu_mem_history.pop(0)

    # Bounding rectangle
    draw.rectangle([(0,0), (127,22)], fill=0, outline=255)

    if currentGraph == show_cpu:
        bar_heights = [round(x*pixel_height/100) for x in cpu_history]
        finalTemperature = 0
        if 'coretemp' in temperatures:
            finalTemperature = temperatures['coretemp'][0].current
        if 'k10temp' in temperatures:
            finalTemperature = temperatures['k10temp'][1].current
        cpu_freq, cpu_min, cpu_max = psutil.cpu_freq()
        draw.text((0, 23), "CPU: {:2.0f}%, {:2.0f}°C, {:4.0f}MHz".format(cpupercent, finalTemperature, cpu_freq), font=font, fill=255)
    elif currentGraph == show_mem:
        bar_heights = [round(x*pixel_height/100) for x in mem_history]
        draw.text((0, 23), "CPU Memory: {:2.0f}% used".format(mempercent), font=font, fill=255)
    elif currentGraph == show_gpu:
        bar_heights = [round(x*pixel_height/100) for x in gpu_history]
        gputemp = currentgpustats['gpu_metrics']['temperature_vrgfx']
        gpu_freq = currentgpustats['gpu_metrics']['average_gfxclk_frequency']
        draw.text((0, 23), "GPU: {:2.0f}%, {:2.0f}°C, {:3.0f}MHz".format(gpupercent, gputemp, gpu_freq), font=font, fill=255)
    elif currentGraph == show_gpu_mem:
        bar_heights = [round(x*pixel_height/100) for x in gpu_mem_history]
        draw.text((0, 23), "GPU Memory: {:2.0f}% used".format(gpumem), font=font, fill=255)
        
    for index, bar_height in enumerate(bar_heights):
        draw.line([(index, 21), (index, 21-bar_height)], fill=1)
    
    currentGraph = (currentGraph + 1) % 4
    
    data = im.tobytes()
    
    # Set up feature report package
    data = bytearray([0x61]) + data + bytearray([0x00])

    dev.send_feature_report(data)

    sleep(3)

dev.close()
