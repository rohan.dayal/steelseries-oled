# steelseries-oled
Python utility for controlling SteelSeries keyboards OLEDs. Based on the fantastic work of https://github.com/edbgon/steelseries-oled.

Currently made only for the SteelSeries Apex 7 / SteelSeries Apex 7 TKL with 128x40 size OLED
Try to change the VID/PID yourself to see if it works with your keyboard.

# Installation
Use pip to install easyhid, pillow and if you want to use the statistics app, psutil. 

Linux requires hidapi to be installed (Fedora: dnf install hidapi, Ubuntu: apt install hidapi). Needs to be run with elevated privileges (sudo) to access devices. For best results, create a service that gets launched at startup. 

Windows (untested) requires the hidapi.dll file which can be downloaded from the zip file here: https://github.com/libusb/hidapi/releases.

# Usage
```
python oled.py image.gif
or
python sysstats.py
  - shows a small bar chart with CPU usage, and CPU live stats such as CPU temperature, CPU frequency and Mem usage
or
python syschart.py
  - shows a historical bar chart with CPU usage, and live CPU usage, CPU temperature, CPU frequency. Alternates every second with a historical bar chart of mem usage and live mem usage.
  where [1-5] is the profile number
```
