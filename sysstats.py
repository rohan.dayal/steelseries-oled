#!/usr/bin/env python3

from PIL import Image, ImageFont, ImageDraw
from easyhid import Enumeration
from time import sleep
import signal
import sys
import psutil

def signal_handler(sig, frame):
    try:
        # Blank screen on shutdown
        dev.send_feature_report(bytearray([0x61] + [0x00] * 641))
        dev.close()
        print("\n")
        sys.exit(0)
    except:
        sys.exit(0)

# Set up ctrl-c handler
signal.signal(signal.SIGINT, signal_handler)

# Stores an enumeration of all the connected USB HID devices
en = Enumeration()
# Return a list of devices based on the search parameters / Hardcoded to Apex 7
devices = en.find(vid=0x1038, pid=0x1612, interface=1)
if not devices:
    devices = en.find(vid=0x1038, pid=0x1618, interface=1)
if not devices:
    print("No devices found, exiting.")
    sys.exit(0)

# Use first device found with vid/pid
dev = devices[0]

print("Found: " + dev.product_string)
print("Press Ctrl-C to exit.\n")
dev.open()

im = Image.new('1', (128,40))
draw = ImageDraw.Draw(im)

while(1):
    # use a truetype font
    draw.rectangle([(0,0),(128,40)], fill=0)
    font = ImageFont.truetype("OpenSans-Regular.ttf", 10)

    cpu_freq, cpu_min, cpu_max = psutil.cpu_freq()
    cpupercent = psutil.cpu_percent(interval=1);
    temperatures = psutil.sensors_temperatures()
    finalTemperature = 0
    if 'coretemp' in temperatures:
        finalTemperature = temperatures['coretemp'][0].current
    if 'k10temp' in temperatures:
        finalTemperature = temperatures['k10temp'][1].current
    
    draw.text((15, 0), "CPU: {:2.0f}%, {:2.0f}°C".format(cpupercent, finalTemperature), font=font, fill=255)
    draw.text((15, 10), "          {:4.0f}MHz".format(cpu_freq), font=font, fill=255)
    draw.text((15, 20), "Memory: {:2.0f}%".format(psutil.virtual_memory().percent), font=font, fill=255)
    draw.rectangle([(3,3), (10,37)], fill=0, outline=255)
    height = round(cpupercent*32/100);
    draw.rectangle([(4,36-height),(9,36)], fill=255, outline=255)

    data = im.tobytes()
    # Set up feature report package
    data = bytearray([0x61]) + data + bytearray([0x00])

    dev.send_feature_report(data)

    sleep(0.1)

dev.close()
